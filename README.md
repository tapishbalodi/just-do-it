# Just-Do-It List Application

# Overview

This is a simple To-Do list application that allows users to manage their tasks. Users can add, delete, edit, and complete tasks. The tasks are categorized into three lists: To Do, Doing, and Done.

## Features

- Add new tasks
- Delete existing tasks
- Edit task details
- Mark tasks as complete
- Sort tasks by priority and end date (sorting functionality can be added as an enhancement)

## Tech Stack

- HTML
- CSS
- Vanilla JavaScript
- Bootstrap for styling and few CDNs

## How to Run

1. Clone the repository:
   ```sh
   git clone https://gitlab.com/tapishbalodi/just-do-it.git
   open index.html or use live server extension in vscode
