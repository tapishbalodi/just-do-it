document.addEventListener('DOMContentLoaded', () => {
  let taskData = fetchTasks();

  const todoList = document.getElementById('todo-list');
  const doingList = document.getElementById('doing-list');
  const doneList = document.getElementById('done-list');

  document.getElementById('add-task-btn').addEventListener('click', addTask);
  document.getElementById('sort-priority').addEventListener('click', () => sortTasks('priority'));

 
  document.getElementById('sort-enddate').addEventListener('click', () => sortTasks('endDate'));

  function fetchTasks() {
    return [
      { id: 1, title: 'Task 1', description: 'Description 1', endDate: '2024-05-25', priority: 1, status: 'todo' },
      { id: 2, title: 'Task 2', description: 'Description 2', endDate: '2024-05-26', priority: 2, status: 'doing' },
      { id: 3, title: 'Task 3', description: 'Description 3', endDate: '2024-05-27', priority: 3, status: 'done' }
    ];
  }

  function renderTasks() {
    clearLists();
    taskData.forEach(task => {
      const taskItem = createTaskElement(task);
      if (task.status === 'todo') {
        todoList.appendChild(taskItem);
      } else if (task.status === 'doing') {
        doingList.appendChild(taskItem);
      } else if (task.status === 'done') {
        doneList.appendChild(taskItem);
      }
    });
  }

  function createTaskElement(task) {
    const taskItem = document.createElement('li');
    taskItem.classList.add('list-group-item');
    taskItem.dataset.id = task.id;
    taskItem.draggable = true;
    taskItem.innerHTML = `
      <span>
        <strong>${task.title}</strong>
        <p>${task.description}</p>
        <p>End Date: ${task.endDate}</p>
        <p>Priority: ${task.priority}</p>
      </span>
      <span>
        <button class="btn btn-success complete-btn">Complete</button>
        <button class="btn btn-warning edit-btn">Edit</button>
        <button class="btn btn-danger delete-btn">Delete</button>
      </span>
    `;

    taskItem.querySelector('.complete-btn').addEventListener('click', () => completeTask(task.id));
    taskItem.querySelector('.edit-btn').addEventListener('click', () => editTask(task.id));
    taskItem.querySelector('.delete-btn').addEventListener('click', () => deleteTask(task.id));

    taskItem.addEventListener('dragstart', dragStart);
    taskItem.addEventListener('dragend', dragEnd);

    return taskItem;
  }

  function clearLists() {
    todoList.innerHTML = '';
    doingList.innerHTML = '';
    doneList.innerHTML = '';
  }

  function addTask() {
    const newTask = {
      id: Date.now(),
      title: 'New Task',
      description: 'New Description',
      endDate: new Date(Date.now()).toDateString(),
      priority: 1,
      status: 'todo'
    };
    taskData.push(newTask);
    renderTasks();
  }

  function deleteTask(id) {
    const index = taskData.findIndex(task => task.id === id);
    if (index !== -1) {
      taskData.splice(index, 1);
      renderTasks();
    }
  }

  function editTask(id) {
    const task = taskData.find(task => task.id === id);
    if (task) {
      const newTitle = prompt('Edit Title:', task.title);
      const newDescription = prompt('Edit Description:', task.description);
      const newDate = prompt('Edit Date:', task.endDate);
      const newPriority = prompt('Edit Priority:', task.priority);
      if (newTitle !== null) task.title = newTitle;
      if (newDescription !== null) task.description = newDescription;
      if (newPriority !== null) task.priority = parseInt(newPriority);
      if (newDate !== null) task.endDate = newDate;
      renderTasks();
    }
  }

  function completeTask(id) {
    const task = taskData.find(task => task.id === id);
    if (task) {
      task.status = 'done';
      renderTasks();
    }
  }

  function sortTasks(criteria,status) {
    if (criteria === 'priority') {
      console.log(taskData.filter(item=>item.status===status))
      taskData.sort((a, b) => a.priority - b.priority);
    } else if (criteria === 'endDate') {
      taskData.sort((a, b) => new Date(a.endDate) - new Date(b.endDate));
    }
    renderTasks();
  }

  window.allowDrop = function(event) {
    event.preventDefault();
  }

  window.dragStart = function(event) {
    event.target.classList.add('dragging');
    event.dataTransfer.setData('text/plain', event.target.dataset.id);
  }

  window.dragEnd = function(event) {
    event.target.classList.remove('dragging');
  }

  window.drop = function(event) {
    event.preventDefault();
    const id = event.dataTransfer.getData('text');
    const task = taskData.find(task => task.id == id);
    const targetList = event.target.closest('ul');

    if (targetList.id === 'todo-list') {
      task.status = 'todo';
    } else if (targetList.id === 'doing-list') {
      task.status = 'doing';
    } else if (targetList.id === 'done-list') {
      task.status = 'done';
    }

    renderTasks();
  }

  renderTasks();
});


document.addEventListener('DOMContentLoaded', () => {
  let taskData = fetchTasks();

  const todoList = document.getElementById('todo-list');
  const doingList = document.getElementById('doing-list');
  const doneList = document.getElementById('done-list');

  document.getElementById('add-task-btn').addEventListener('click', addTask);
  document.getElementById('sort-priority').addEventListener('click', () => sortTasks('priority'));
  document.getElementById('sort-enddate').addEventListener('click', () => sortTasks('endDate'));

  const editTaskForm = document.getElementById('edit-task-form');
  editTaskForm.addEventListener('submit', saveTaskChanges);

  function fetchTasks() {
    return [
      { id: 1, title: 'Task 1', description: 'Description 1', endDate: new Date(Date.now()).toDateString(), priority: 1, status: 'todo' },
      { id: 2, title: 'Task 2', description: 'Description 2', endDate: new Date(Date.now()).toDateString(), priority: 2, status: 'doing' },
      { id: 3, title: 'Task 3', description: 'Description 3', endDate: new Date(Date.now()).toDateString(), priority: 3, status: 'done' }
    ];
  }

  function renderTasks() {
    clearLists();
    taskData.forEach(task => {
      const taskItem = createTaskElement(task);
      if (task.status === 'todo') {
        todoList.appendChild(taskItem);
      } else if (task.status === 'doing') {
        doingList.appendChild(taskItem);
      } else if (task.status === 'done') {
        doneList.appendChild(taskItem);
      }
    });
  }

  function createTaskElement(task) {
    const taskItem = document.createElement('li');
    taskItem.classList.add('list-group-item');
    taskItem.dataset.id = task.id;
    taskItem.draggable = true;
    taskItem.innerHTML = `
      <span>
        <strong>${task.title}</strong>
        <p>${task.description}</p>
        <p>End Date: ${task.endDate}</p>
        <p>Priority: ${task.priority}</p>
      </span>
      <span>
        <button class="btn btn-success complete-btn">Complete</button>
        <button class="btn btn-warning edit-btn" data-toggle="modal" data-target="#editTaskModal">Edit</button>
        <button class="btn btn-danger delete-btn">Delete</button>
      </span>
    `;

    taskItem.querySelector('.complete-btn').addEventListener('click', () => completeTask(task.id));
    taskItem.querySelector('.edit-btn').addEventListener('click', () => openEditModal(task.id));
    taskItem.querySelector('.delete-btn').addEventListener('click', () => deleteTask(task.id));

    taskItem.addEventListener('dragstart', dragStart);
    taskItem.addEventListener('dragend', dragEnd);

    return taskItem;
  }

  function clearLists() {
    todoList.innerHTML = '';
    doingList.innerHTML = '';
    doneList.innerHTML = '';
  }

  function addTask() {
    const newTask = {
      id: Date.now(),
      title: 'New Task',
      description: 'New Description',
      endDate: new Date(Date.now()).toDateString(),
      priority: 1,
      status: 'todo'
    };
    taskData.push(newTask);
    renderTasks();
  }

  function deleteTask(id) {
    const index = taskData.findIndex(task => task.id === id);
    if (index !== -1) {
      taskData.splice(index, 1);
      renderTasks();
    }
  }

  function openEditModal(id) {
    const task = taskData.find(task => task.id === id);
    if (task) {
      document.getElementById('task-id').value = task.id;
      document.getElementById('task-title').value = task.title;
      document.getElementById('task-description').value = task.description;
      document.getElementById('task-end-date').value = task.endDate;
      document.getElementById('task-priority').value = task.priority;
    }
  }

  function saveTaskChanges(event) {
    event.preventDefault();
    const id = document.getElementById('task-id').value;
    const title = document.getElementById('task-title').value;
    const description = document.getElementById('task-description').value;
    const endDate = document.getElementById('task-end-date').value;
    const priority = document.getElementById('task-priority').value;

    const task = taskData.find(task => task.id == id);
    if (task) {
      task.title = title;
      task.description = description;
      task.endDate = endDate;
      task.priority = parseInt(priority);
      renderTasks();
    }

    $('#editTaskModal').modal('hide');
  }

  function completeTask(id) {
    const task = taskData.find(task => task.id === id);
    if (task) {
      task.status = 'done';
      renderTasks();
    }
  }

  function sortTasks(criteria) {
    if (criteria === 'priority') {
      taskData.sort((a, b) => a.priority - b.priority);
    } else if (criteria === 'endDate') {
      taskData.sort((a, b) => new Date(a.endDate) - new Date(b.endDate));
      console.log(taskData)
    }
    renderTasks();
  }

  window.allowDrop = function(event) {
    event.preventDefault();
  }

  window.dragStart = function(event) {
    event.target.classList.add('dragging');
    event.dataTransfer.setData('text/plain', event.target.dataset.id);
  }

  window.dragEnd = function(event) {
    event.target.classList.remove('dragging');
  }

  window.drop = function(event) {
    event.preventDefault();
    const id = event.dataTransfer.getData('text');
    const task = taskData.find(task => task.id == id);
    const targetList = event.target.closest('ul');

    if (targetList.id === 'todo-list') {
      task.status = 'todo';
    } else if (targetList.id === 'doing-list') {
      task.status = 'doing';
    } else if (targetList.id === 'done-list') {
      task.status = 'done';
    }

    renderTasks();
  }

  renderTasks();
});
